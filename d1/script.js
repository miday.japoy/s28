/*console.log(`Hello World`);
console.log(`Hello Again`);	

for(let i = 0; i <= 1500; i++){
	console.log(i)
};

console.log(`The End`);
*/


// Asynchronous
	// means that we can proceed to execute other statements, while time consuming codes is running in the background.

//Getting all posts
  /*

	Syntax:
		fetch('URL')
		console.log(fetch())


  */ 

fetch('https://jsonplaceholder.typicode.com/posts');
	// allows you to asynchronously fetch data
		console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	/*
		Syntax:
		fetch('URL')
		.then(response) => ())
	*/



	// fetch('https://jsonplaceholder.typicode.com/posts')
	// 	.then((response) => console.log(response.status))	

		// captures the response object and return another promise which will eventually be resolved or rejected.

	// fetch('https://jsonplaceholder.typicode.com/posts')
	// 	.then((response) => response.json())

		//json method from response object to convert the data retrieved into JSON format to be used in our application.

		// .then((json) => console.log(json))

		// print the converted JSON value from the fetch request

		//using multiple ".then" methods creates a "promise chain"

		// Async- Await

/*
		async function fetchData(){
			let result = await fetch('https://jsonplaceholder.typicode.com/posts')

			console.log(result);

			console.log(typeof result);

			console.log(result.body);

			let json = await result.json();

			console.log(json);
		}

		console.log(fetchData());
*/

	//Syntactic Sugar
			//more readable
			//but not necessarily better

	// Async-Await is the syntactic sugar of .then method.

		//Retrieve specific post
		fetch('https://jsonplaceholder.typicode.com/posts/14')
		.then((response) => response.json())
		.then((json) => console.log(json))


	//creating a post

	//syntax:
		//fetch('URL', options)
		//.then((response) => {})
		//.then((response) => {})


// add data

fetch('https://jsonplaceholder.typicode.com/posts', {
	method:'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World',
		userID: 1
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

//update data
fetch('https://jsonplaceholder.typicode.com/posts/10', {
	method:'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 10,
		title: 'Updated Post',
		body: 'Hello World',
		userID: 1
		
	})
})

.then((response) => response.json())
.then((json) => console.log(json))


//Update post with PATCH - for updating

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method:'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated Post(1)',
		
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

// PUT and PATCH
	//both deals with updates
	//The number of properties being changed


// Delete



// fetch('https://jsonplaceholder.typicode.com/posts/1', {
// 	method:'DELETE',
// })



fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json))